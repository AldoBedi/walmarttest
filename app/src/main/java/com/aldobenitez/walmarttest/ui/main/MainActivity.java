package com.aldobenitez.walmarttest.ui.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aldobenitez.walmarttest.R;
import com.aldobenitez.walmarttest.common.LoadingDialog;
import com.aldobenitez.walmarttest.common.Util;
import com.aldobenitez.walmarttest.data.object.Product;

public class MainActivity extends AppCompatActivity implements MainInterface.View, View.OnClickListener{

    /*  CONSTANTS   */

    /*  UI ELEMENTS   */
    private ImageView imgProduct;
    private TextView tvProductName;
    private TextView tvProductPrice;
    private TextView tvProductCode;
    private TextView tvProductDepartment;
    private Button btnGetInfo;
    private View lineDivider;

    /*  INSTANCES   */
    private LoadingDialog loading;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();
        initViews();
    }

    private void bindViews(){
        imgProduct = findViewById(R.id.imgProduct);
        tvProductName = findViewById(R.id.tvProductName);
        tvProductPrice = findViewById(R.id.tvProductPrice);
        tvProductDepartment = findViewById(R.id.tvProductDepartment);
        tvProductCode = findViewById(R.id.tvProductCode);
        lineDivider = findViewById(R.id.viewLine);
        btnGetInfo = findViewById(R.id.btnGetInfo);
        btnGetInfo.setOnClickListener(this);
    }

    private void initViews(){
        presenter = new MainPresenter(this, this);
    }

    @Override
    public void showLoading(String message) {
        loading = LoadingDialog.newInstance(message);
        loading.setCancelable(false);
        loading.show(getSupportFragmentManager() , LoadingDialog.TAG);
    }

    @Override
    public void dismissLoading() {
        if(loading != null)
            loading.dismiss();
    }

    @Override
    public void showProductInformacion(Product product) {
        if(product != null){
            imgProduct.setVisibility(View.VISIBLE);
            lineDivider.setVisibility(View.VISIBLE);
            btnGetInfo.setVisibility(View.GONE);
            tvProductName.setText(product.getSkuDisplayNameText());
            tvProductPrice.setText(Util.getCurriencyValue(Util.getDoubleValue(product.getBasePrice())));
            tvProductCode.setText(getString(R.string.code)+" "+product.getSkuId());
            tvProductDepartment.setText(getString(R.string.department)+" "+product.getDepartment());
        }
    }

    @Override
    public void showErrorInformation(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnGetInfo){
            presenter.getProductInformation();
        }
    }
}
