package com.aldobenitez.walmarttest.ui.main;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.aldobenitez.walmarttest.R;
import com.aldobenitez.walmarttest.common.Constants;
import com.aldobenitez.walmarttest.common.Util;
import com.aldobenitez.walmarttest.data.object.Product;
import com.aldobenitez.walmarttest.services.VolleyManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by asbd1 on 12/02/2019.
 */

public class MainInteractor implements MainInterface.Interactor{

    /*  INSTANCES   */
    private MainPresenter presenter;
    private VolleyManager volleyManager;
    private Context context;

    public MainInteractor(MainPresenter presenter, Context context){
        this.presenter = presenter;
        this.context = context;
        volleyManager = VolleyManager.getInstance(context);

    }

    @Override
    public void callServiceInformation() {
        doProductGetRequest();
    }

    /**
     * SE INVOCA EL SERVICIO QUE OBTIENE LA INFORMACION DEL PRODUCTO
     * */
    public void doProductGetRequest(){
        String url = Constants.SERVER+Constants.GET_PRODUCT_INFO;
        if(Util.isNetworkAvailable(context)){
            volleyManager.setOnRequestListener(new VolleyManager.OnRequestListener() {
                @Override public void onRequestSuccess(JSONArray responseArray, String method) {}

                @Override
                public void onRequestSuccess(JSONObject responseObject, String method) throws JSONException {
                    Product product = new Gson().fromJson(responseObject.toString(), Product.class);
                    presenter.callBackInformation(product);
                }

                @Override
                public void onRequestFail(String errorMessage) {
                    Log.e(Constants.TAG_APP, "Error response: "+errorMessage);
                    presenter.callBackError(errorMessage);
                }
            });
            volleyManager.executeGetRequest(url);
        }else{
            presenter.callBackError(context.getResources().getString(R.string.msg_no_internet_connection));
        }
    }

}
