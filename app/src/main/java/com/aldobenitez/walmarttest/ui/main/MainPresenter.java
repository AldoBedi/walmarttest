package com.aldobenitez.walmarttest.ui.main;

import android.content.Context;

import com.aldobenitez.walmarttest.R;
import com.aldobenitez.walmarttest.data.object.Product;

/**
 * Created by asbd1 on 12/02/2019.
 */

public class MainPresenter implements MainInterface.Presenter {

    /*  INSTANCES   */
    private MainInterface.View view;
    private MainInterface.Interactor interactor;
    private Context context;


    public MainPresenter(MainInterface.View view, Context context){
        this.view = view;
        this.context = context;
        interactor = new MainInteractor(this, context);
    }

    @Override
    public void getProductInformation() {
        view.showLoading(context.getResources().getString(R.string.msg_wait));
        interactor.callServiceInformation();
    }

    @Override
    public void callBackInformation(Product product) {
        view.dismissLoading();
        view.showProductInformacion(product);
    }

    @Override
    public void callBackError(String message) {
        view.dismissLoading();
        view.showErrorInformation(message);
    }

}
