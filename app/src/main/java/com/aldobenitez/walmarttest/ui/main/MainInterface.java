package com.aldobenitez.walmarttest.ui.main;

import com.aldobenitez.walmarttest.data.object.Product;

/**
 * Created by asbd1 on 12/02/2019.
 */

public interface MainInterface {

    interface View {
        void showLoading(String message);
        void dismissLoading();
        void showProductInformacion(Product product);
        void showErrorInformation(String errorMessage);
    }

    interface Presenter{
        void getProductInformation();
        void callBackInformation(Product product);
        void callBackError(String message);
    }

    interface Interactor{
        void callServiceInformation();
    }
}
