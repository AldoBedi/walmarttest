package com.aldobenitez.walmarttest.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by asbd1 on 13/02/2019.
 */

public class Util {


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static double getRoundValue(double value) {
        NumberFormat nf = new DecimalFormat("0.00");
        nf.setMinimumIntegerDigits(1);
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        nf.setRoundingMode(RoundingMode.HALF_UP);
        value = Double.valueOf(nf.format(value).replace(",", ""));
        return value;
    }

    public static double getDoubleValue(String value) {
        try {
            value = value.replace("$", "").replace(",", "").replace("%", "")
                    .replace("&nbsp;", "");
            if (value.isEmpty()) {
                return 0;
            }
            return Double.valueOf(value);
        } catch (Exception e) {
            return 0;
        }
    }



    public static String getCurriencyValue(double value) {
        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
        return defaultFormat.format(getRoundValue(value));
    }


}
