package com.aldobenitez.walmarttest.common;

/**
 * Created by asbd1 on 12/02/2019.
 */

public class Constants {

    public static final String TAG_APP = "WalmartTest";

    //=== Datos de servicios
    public static final String SERVER = "https://super.walmart.com.mx/api/rest/model/atg/commerce/";
    public static final String GET_PRODUCT_INFO = "catalog/ProductCatalogActor/getSkuSummaryDetails?storeId=0000009999&upc=00750129560012&skuId=00750129560012";

    //==Configuracion de peticiones
    public static final int TIMEOUT = 20000;
    public static final int RETRIES = 1;


}
