package com.aldobenitez.walmarttest.common;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aldobenitez.walmarttest.R;


public class LoadingDialog extends DialogFragment {

    /*  VARIABLES  */
    public static final String TAG = "Loading";
    private static final String MESSAGE = "mesage";
    private String message;

    /*  UI ELEMENTS */
    private View rootView;

    public LoadingDialog() {
        // Required empty public constructor
    }

    public static LoadingDialog newInstance(String message) {
        LoadingDialog fragment = new LoadingDialog();
        Bundle args = new Bundle();
        args.putString(MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(MESSAGE, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_loading_dialog, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView textLoading = (TextView) rootView.findViewById(R.id.textLoading);
        if (message != null) textLoading.setText(message);
        setCancelable(false);
        return rootView;
    }

}
