package com.aldobenitez.walmarttest.services;

import android.content.Context;
import android.util.Log;

import com.aldobenitez.walmarttest.common.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


public class VolleyManager implements Response.Listener<JSONObject>, Response.ErrorListener {

    private static final String TAG_SERVICIOS = "TAG_SERVICIOS";
    HurlStack hurlStack = null;


    /**
     * Interface
     */
    public interface OnRequestListener {
        void onRequestSuccess(JSONArray responseArray, String method);

        void onRequestSuccess(JSONObject responseObject, String method) throws JSONException;

        void onRequestFail(String errorMessage);
    }

    /**
     * Member Variables
     */
    private Context context;
    private OnRequestListener onRequestListener;
    private HashMap<String, String> headers;

    /**
     * Static Variables
     */
    private static RequestQueue sRequestQueue;
    private static boolean necesitaCertificado = true;
    private static final String TAG = VolleyManager.class.getSimpleName();
    private static final String JSON_TYPE = "application/json; charset=utf-8";
    private static final String X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded; charset=UTF-8";
    private static final String DATA = "data";
    private static final String METHOD = "method";

    /**
     * Constructors
     */
    private VolleyManager(Context context) {
        this.context = context;
        headers = new HashMap<>();
    }


    /**
     * Factory Method
     */
    public static VolleyManager getInstance(Context context, HttpStack httpStack) {
        if (necesitaCertificado) {
            necesitaCertificado = false;
            synchronized (VolleyManager.class) {
                sRequestQueue = Volley.newRequestQueue(context.getApplicationContext(), httpStack);
                sRequestQueue.start();
            }
        }
        return new VolleyManager(context);
    }


    public static VolleyManager getInstance(Context context) {
        synchronized (VolleyManager.class) {
            necesitaCertificado = true;
            sRequestQueue = Volley.newRequestQueue(context);
            //sRequestQueue.start();
        }
        return new VolleyManager(context);
    }


    @Deprecated
    public static VolleyManager getInstanceHurlStack(Context context, HurlStack hurlStack) {
        if (sRequestQueue == null) {
            synchronized (VolleyManager.class) {
                Cache cache = new DiskBasedCache(context.getCacheDir(), -1);
                // Set up the network to use HttpURLConnection as the HTTP client.
                Network network = new BasicNetwork(hurlStack);
                sRequestQueue = new RequestQueue(cache, network);
                sRequestQueue.start();
            }
        }
        return new VolleyManager(context);
    }



    /**
     * Getters and Setters
     */
    public OnRequestListener getOnRequestListener() {
        return onRequestListener;
    }

    public void setOnRequestListener(OnRequestListener onRequestListener) {
        this.onRequestListener = onRequestListener;
    }




    public HashMap<String, String> getRequestJsonHeaders() {
        headers.clear();
        headers.put("Content-Type", JSON_TYPE);
        headers.put("Accept", JSON_TYPE);
        return headers;
    }


    public void executeGetRequest(String url) {
        JsonRequest<JSONObject> request = new JsonRequest<JSONObject>(Request.Method.GET, url, null, this, this) {

            @Override
            public String getBodyContentType() {
                return JSON_TYPE;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getRequestJsonHeaders();
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString =
                            new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(METHOD, "GoogleZipCode");
                        jsonObject.put(DATA, jsonString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return Response.success(jsonObject, HttpHeaderParser.parseCacheHeaders(response));

                } catch (UnsupportedEncodingException e) {
                    System.out.println("UnsupportedEncodingException: " + e.toString());
                    return Response.error(new ParseError(e));
                }
            }
        };
        request.setTag(TAG_SERVICIOS);
        request.setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        sRequestQueue.add(request);
    }

    private JSONArray toJSONArray(String responseObject) {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(responseObject);
        } catch (JSONException e) {
            System.out.println("JSONException: " + e.toString());
        }
        return jsonArray;
    }

    private JSONObject toJSONObject(String responseObject) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(responseObject);
        } catch (JSONException e) {
            System.out.println("toJSONObject: " + e.toString());
        }

        return jsonObject;
    }

    // Response.Listener
    @Override
    public void onResponse(JSONObject response) {
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;
        String responseObject = null;
        String method = null;
        try {
            method = response.getString(METHOD);
            responseObject = response.getString(DATA);
        } catch (JSONException e) {
            System.out.println("JSONException: " + e.toString());
        }

        if ((jsonArray = toJSONArray(responseObject)) != null) {
            if (onRequestListener != null) {
                onRequestListener.onRequestSuccess(jsonArray, method);
            }
        } else if ((jsonObject = toJSONObject(responseObject)) != null) {
            if (onRequestListener != null) {

                try {
                    onRequestListener.onRequestSuccess(jsonObject, method);
                } catch (JSONException e) {
                    System.out.println("JSONException: " + e.toString());

                    String errorMessage = String.format("%s Error en la operacion: ", method);
                    e.printStackTrace();
                    if (onRequestListener != null) {
                        onRequestListener.onRequestFail(errorMessage);
                }
                }

            }
        } else {
            String errorMessage = String.format("%s couldn't be parsed as a valid json object", responseObject);
            Error error = new Error(errorMessage);

            if (onRequestListener != null) {
                onRequestListener.onRequestFail(errorMessage);
            }
        }
    }

    // Response.ErrorListener
    @Override
    public void onErrorResponse(VolleyError volleyError) {
        NetworkResponse response = volleyError.networkResponse;
        if (volleyError instanceof ServerError && response != null) {
            try {
                String res = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                // Now you can use any deserializer to make sense of data
                //JSONObject obj = new JSONObject(res);
                String messageError ="";
                if(res!=null && !res.equals("")){
                    messageError = res;
                }else{
                    messageError = "Ocurrió un error al procesar la solicitud, intente nuevamente";
                }
                if (onRequestListener != null) {
                    onRequestListener.onRequestFail(messageError);
                }
            } catch (UnsupportedEncodingException e1) {
                // Couldn't properly decode data to string
                e1.printStackTrace();
            }
        }
    }

}